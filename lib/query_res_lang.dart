import 'dart:convert';

import 'package:http/http.dart' as http;

String mediawiki_host = 'www.mediawiki.org';

Future<String> queryResLang(String lang) async {
  final response = await http.get(Uri.https("${mediawiki_host}",
      '/w/api.php',
      {
        'action': 'languagesearch',
        'format': 'json',
        'search': lang
      }));
  if (response.statusCode == 200) {
    Map resJson = json.decode(response.body);
    return resJson["languagesearch"].keys.first;
  } else {
    throw Exception(
        'Failed to query ${mediawiki_host}: ${response.statusCode}');
  }
}
