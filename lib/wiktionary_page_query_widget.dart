import 'package:flutter/material.dart';

import 'wiktionary_page_widget.dart';

class WiktionaryPageQueryWidget extends StatefulWidget {
  @override
  _WiktionaryPageQueryWidgetState createState() =>
      new _WiktionaryPageQueryWidgetState();
}

class _WiktionaryPageQueryWidgetState extends State<WiktionaryPageQueryWidget> {
  String resLang;
  String lang;
  String word;

  @override
  void initState() {
    super.initState();
    this.resLang = "Suomi";
    this.lang = "Suomi";
    this.word = "puhua";
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        TextFormField(
          decoration: InputDecoration(
            //border: InputBorder.none,
            hintText: 'Result Language',
          ),
          initialValue: this.resLang,
          onChanged: (text) {
            setState(() => this.resLang = text);
          },
        ),
        TextFormField(
          decoration: InputDecoration(
            //border: InputBorder.none,
            hintText: 'Language',
          ),
          initialValue: this.lang,
          onChanged: (text) {
            setState(() => this.lang = text);
          },
        ),
        TextFormField(
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Word',
          ),
          initialValue: this.word,
          onChanged: (text) {
            setState(() => this.word = text);
          },
        ),
        RaisedButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (BuildContext context) {
                  return Scaffold(
                    appBar: AppBar(
                      title: Text("${this.lang}: ${this.word}"),
                    ),
                    body: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Center(
                        child: WiktionaryPageWidget(
                            this.word, this.lang, this.resLang),
                      ),
                    ),
                  );
                },
              ),
            );
          },
          child: const Text(
            'Query',
            //style: TextStyle(fontSize: 20)
          ),
        ),
      ],
    );
  }
}
