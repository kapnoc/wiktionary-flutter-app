import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import 'wiktionary_page.dart';

class WiktionaryPageWidget extends StatefulWidget {
  String _word;
  String _lang;
  String _resLangFull;

  WiktionaryPageWidget(String word, String lang, String resLangFull) {
    this._word = word;
    this._lang = lang;
    this._resLangFull = resLangFull;
  }

  @override
  _WiktionaryPageWidgetState createState() => new _WiktionaryPageWidgetState();
}

class _WiktionaryPageWidgetState extends State<WiktionaryPageWidget> {
  Future<WiktionaryPage> futureWiktionaryPage;
  String pageName;

  @override
  void initState() {
    super.initState();
    futureWiktionaryPage =
        fetchWiktionaryPage(widget._word, widget._lang, widget._resLangFull);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WiktionaryPage>(
      future: futureWiktionaryPage,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return SingleChildScrollView(child: Html(
            data: snapshot.data.text,
            //padding: EdgeInsets.all(15.0)
          ));
          //return SingleChildScrollView(child: Text(snapshot.data.text));
        } else if (snapshot.hasError) {
          return Text("Error querying wiktionary: ${snapshot.error}");
        }
        // By default, show a loading spinner.
        return CircularProgressIndicator();
      },
    );
  }
}
