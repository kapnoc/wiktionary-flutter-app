import 'dart:convert';

import 'package:http/http.dart' as http;

String wiktionary_host = 'wiktionary.org';

Future<Wikitext> fetchWikitext(String word, String resLang) async {
  final response = await http.get(Uri.https("${resLang}.${wiktionary_host}",
      '/w/api.php',
      {
        'action': 'parse',
        'format': 'json',
        'prop': 'wikitext|sections',
        'page': word
      }));
  if (response.statusCode == 200) {
    return Wikitext.fromJson(json.decode(response.body));
  } else {
    throw Exception(
        'Failed to query ${wiktionary_host}: ${response.statusCode}');
  }
}

class WikitextSection {
  final String line;
  final int toclevel;
  final int byteoffset;

  WikitextSection({this.line, this.toclevel, this.byteoffset});

  factory WikitextSection.fromJson(Map<String, dynamic> json) {
    return WikitextSection(
      line: json['line'],
      toclevel: json['toclevel'],
      byteoffset: json['byteoffset']
    );
  }
}

class Wikitext {
  final String title;
  final String text;
  final List<WikitextSection> sections;

  Wikitext({this.title, this.text, this.sections});

  factory Wikitext.fromJson(Map<String, dynamic> json) {
    return Wikitext(
        title: json['parse']['title'],
        text: json['parse']['wikitext']['*'],
        sections: (json['parse']['sections'] as List).map((sectJson) => WikitextSection.fromJson(sectJson)).toList()
    );
  }
}
