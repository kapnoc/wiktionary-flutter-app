import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:html/parser.dart' show parse;
import 'package:html/dom.dart';

import 'wikitext.dart';
import 'query_res_lang.dart';

Future<WiktionaryPage> fetchWiktionaryPage(String word, String lang,
    String resLangFull) async {
  String resLang = await queryResLang(resLangFull);
  final wikitext = await fetchWikitext(word, resLang);
  final response = await http.get(Uri.https("${resLang}.${wiktionary_host}",
      '/w/api.php',
      {
        'action': 'parse',
        'format': 'json',
        'prop': 'text',
        'disabletoc': '',
        'page': word
        //'title': wikitext.title,
        //'text': wikitext.text
      }));
  if (response.statusCode == 200) {
    return WiktionaryPage.fromJson(json.decode(response.body), lang);
  } else {
    throw Exception(
        'Failed to query ${wiktionary_host}: ${response.statusCode}');
  }
}

class WiktionaryPage {
  final String text;

  WiktionaryPage({this.text});

  factory WiktionaryPage.fromJson(Map<String, dynamic> json, String lang) {
    final String textHtml = json['parse']['text']['*'];
    final Element textDoc = Element.html(textHtml);
    String cutText = "";
    bool found = false;
    int aled = 0;
    for (var elem in textDoc.children) {
      aled++;
      if (elem.localName == "h2") {
        if (found)
          break;
        if (elem.children[0].id == lang) {
          found = true;
          continue;
        }
      }
      if (found)
        cutText = cutText + elem.outerHtml;
    }
    final Document doc = parse(cutText);
    return WiktionaryPage(
        text: cutText
      //text: doc.outerHtml
      //text: "'${textDoc.children[0].children[0].id}'"
      //text: textHtml
    );
  }
}
