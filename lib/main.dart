// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:flutter/material.dart';

import 'wiktionary_page_query_widget.dart';

void main() => runApp(WiktionaryApp());

class WiktionaryApp extends StatefulWidget {
  WiktionaryApp({Key key}) : super(key: key);

  @override
  _WiktionaryAppState createState() => _WiktionaryAppState();
}

class _WiktionaryAppState extends State<WiktionaryApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wiktionary',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Wiktionary'),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Center(
              child: WiktionaryPageQueryWidget()
          ),
        ),
      ),
    );
  }
}
